# listwithinlist 
movies = [
	"The Holy Grail", 1975, "Terry Jones & Terry Gilliam", 91,
		["Graham Champman",
			["Michael Palin","John Cleese","Terry Gilliam","Eric Idle","Terry Jones"]
		]
]

print (len(movies))

# if i print this , the answer The holy grail

print (movies[0])

print movies[1]
# 1975

print (movies[2])
# Terry Jones & Terry Gilliam


# Now it's print h because at first 0 means first list and 1 means h.
# That means go to first list and give me index one answer.

print (movies[0][1])
# h

print (movies[4])
# ['Graham Champman', ['Michael Palin', 'John Cleese', 'Terry Gilliam', 'Eric Idle', 'Terry Jones']]

print (movies[4][1])
# ['Michael Palin', 'John Cleese', 'Terry Gilliam', 'Eric Idle', 'Terry Jones']

print (movies[4][1][0])
# Michael Palin
